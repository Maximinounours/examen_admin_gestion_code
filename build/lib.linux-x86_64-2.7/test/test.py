#!usr/bin/env python 
#coding : utf-8
"""Module de test de Grid de Space.py"""

import unittest
from Robot import Space

class TestAddPosition(unittest.TestCase):
    """Test de la methode add_position"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.Grid()

    def test_valide(self):
        """bon fonctionnement"""
        self.assertEqual(self.var_grid.add_position("id", 1.0, 2.0, 3.0), 0)


    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.add_position(1, 2, 3, 4), -3)

    def test_arg_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.add_position("id1", 2, 3, 4), -3)
        self.assertEqual(self.var_grid.add_position("id1", 2, "3", 4), -3)
        self.assertEqual(self.var_grid.add_position("id1", 2, 3, '5'), -3)

    def test_id_deja_existant(self):
        """mauvais fonctionnement"""
        self.var_grid.add_position("id", 1.0, 2.0, 3.0)
        self.assertEqual(self.var_grid.add_position("id", 2.0, 3.0, 3.0), -1)

        #Remet le dictionnaire vide
        self.var_grid.entites.pop("id")

    def test_rayon_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.add_position("id", 2.0, 3.0, -3.0), -2)
        self.assertEqual(self.var_grid.add_position("id", 2.0, 3.0, 0.0), -2)

class TestRemovePosition(unittest.TestCase):
    """Test de la methode remove_position"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.Grid()
        self.var_grid.add_position("id", 1.0, 2.0, 5.0)

    def test_valide(self):
        """bon fonctionnement"""
        self.assertEqual(self.var_grid.remove_position("id"), 0)

    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.remove_position(54), -1)

    def test_id_inexistant(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.remove_position("idd"), -2)

class TestListPosition(unittest.TestCase):
    """Test de la methode list_position"""
    def setUp(self):
        """setup"""
        self.var_grid1 = Space.Grid()
        self.var_grid = Space.Grid()
        self.reponse1 = "id2 : (1.0, 2.0, 5.0)" + '\n' + "id : (1.0, 2.0, 5.0)" + '\n'
        self.reponse2 = "Aucune position enregistree"
        self.var_grid.add_position("id", 1.0, 2.0, 5.0)
        self.var_grid.add_position("id2", 1.0, 2.0, 5.0)

    def test_valide(self):
        """bon fonctionnement"""
        self.assertEqual(self.var_grid.list_position(), self.reponse1)

    def test_vide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid1.list_position(), self.reponse2)

class TestGetPosition(unittest.TestCase):
    """Test de la methode get_position"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.Grid()
        self.var_grid.add_position("id", 1.0, 2.0, 5.0)

    def test_valide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.get_position("id"), (1.0, 2.0))

    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.get_position(55), -1)
        self.assertEqual(self.var_grid.get_position("idd"), None)

class TestGetDistance(unittest.TestCase):
    """Test de la methode get_distance"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.Grid()
        self.var_grid.add_position("id", 1.0, 2.0, 5.0)
        self.var_grid.add_position("id0", 1.0, 2.0, 5.0)
        self.var_grid.add_position("id2", 12.0, 2.0, 5.0)

    def test_valide(self):
        """bon fonctionnement"""
        self.assertEqual(self.var_grid.get_distance("id", "id2"), 11)
        self.assertEqual(self.var_grid.get_distance("id2", "id"), 11)
        self.assertEqual(self.var_grid.get_distance("id", "id0"), 0.0)
        self.assertEqual(self.var_grid.get_distance("id", "id"), 0.0)


    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.get_distance(56, "id2"), None)
        self.assertEqual(self.var_grid.get_distance("id", 456), None)
        self.assertEqual(self.var_grid.get_distance("id3", "id2"), None)
        self.assertEqual(self.var_grid.get_distance("id", "idd"), None)

class TestIsCollision(unittest.TestCase):
    """Test de la methode is_collision"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.Grid()
        self.var_grid.add_position("id", 1.0, 2.0, 5.0)
        self.var_grid.add_position("id2", 5.0, 2.0, 5.0)
        self.var_grid.add_position("id0", 12.0, 2.0, 5.0)

    def test_valide(self):
        """bon fonctionnement"""
        self.assertEqual(self.var_grid.is_collision("id", "id2"), True)
        self.assertEqual(self.var_grid.is_collision("id", "id0"), False)
        self.assertEqual(self.var_grid.is_collision("id2", "id"), True)
        self.assertEqual(self.var_grid.is_collision("id0", "id"), False)

    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.is_collision(56, "id2"), -1)
        self.assertEqual(self.var_grid.is_collision("id", 456), -1)
        self.assertEqual(self.var_grid.is_collision("id3", "id2"), -1)
        self.assertEqual(self.var_grid.is_collision("id", "idd"), -1)

if __name__ == "__main__":
    unittest.main()
