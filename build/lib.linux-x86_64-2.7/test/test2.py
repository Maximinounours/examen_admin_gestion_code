#!usr/bin/env python 
#coding : utf-8
"""Module de test de SafeGrid de Space.py"""

import unittest
from Robot import Space

class TestSafeAddPosition(unittest.TestCase):
    """Test de la methode Safe_add_position"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.SafeGrid()

    def test_valide(self):
        """bon fonctionnement"""
        self.assertEqual(self.var_grid.Safe_add_position("id", 1.0, 2.0, 3.0), 0)
        self.var_grid.Safe_add_position("id", 1.0, 2.0, 3.0)
        self.assertEqual(self.var_grid.Safe_add_position("id2", 10.0, 2.0, 3.0), 0)


    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.var_grid.Safe_add_position("id", 1.0, 2.0, 3.0)
        self.assertEqual(self.var_grid.Safe_add_position("id2", 2.0, 2.0, 3.0), -1)


class TestSafeNewPosition(unittest.TestCase):
    """Test de la methode Safe_new_position"""
    def setUp(self):
        """setup"""
        self.var_grid = Space.SafeGrid()
        self.var_grid.Safe_add_position("id", 1.0, 2.0, 3.0)
        self.var_grid.Safe_add_position("id2", 10.0, 2.0, 3.0)


    def test_valide(self):
        """bon fonctionnement"""
        print(self.var_grid.list_position())
        self.assertEqual(self.var_grid.Safe_new_position("id", 1.0, 2.0), 0)


    def test_id_invalide(self):
        """mauvais fonctionnement"""
        self.assertEqual(self.var_grid.Safe_new_position("id", 9.0, 2.0), -1)

if __name__ == "__main__":
    unittest.main()
