#!usr/bin/env python 
#coding : utf-8

from setuptools import setup

setup (
    name="Robot",
    version='0.0.1',
    author="Maxime Leriche",
    description="Systeme de gestion de position avec quelques methodes d'operations sur ces positions",
    license="GNU GPLv3",
    python_requires='>=2.4',
    packages=['Robot','test'],
)