# Auteur
Package réalisé par Maxime LERICHE dans le cadre de l'Examen d’Administration et gestion  des codes

# Contenu du package

Le package contient un module nommé Space, contenant deux classes.
Une première Grid permettant l'enregistrement de positions et une gestion de collision.
Une autre SafeGrid héritant de la première proposant des méthodes d'ajout ou de changement de position tenant compte des collisions engendrées.

# Installation du package

Cloner le projet GitLab, puis effectuer pip install . apres s'être placé dans le répértoire du projet dans un terminal
```
> git clone https://gitlab.com/Maximinounours/examen_admin_gestion_code.git
> cd examen_admin_gestion_code
> pip install .
```

# 
