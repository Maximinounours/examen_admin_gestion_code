#!usr/bin/env python 
#coding : utf-8
"""Module contenant une classe permettant de gerer des positions dans un espace 2D"""
class Grid:
    """Classe permettant de stocker la position et le rayon
    d'entites identifies par leur id.
    On peut ajouter, retirer et realiser des operations (comparer,
    la distance, collision...) entre plusieurs entites
    Les entites sont dans un dictionnaire dont la clef est l'id
    et la valeur est la position (x et y) et le rayon (r) :
    {'id1': (x,y,r), 'id2': ...} avec
    x, y et r des flottants
    """
    def __init__(self):
        """Constructeur vide"""
        #Creer un dictionnaire vide que l'on va remplir apres
        self.entites = {}


    def add_position(self, var_id, var_x, var_y, var_r):
        """Prend un str et trois float
        Ajoute au dictionnaire de positions la position
        en argument avec l'id speicife"""
        #On verifie avoir des float
        if (not isinstance(var_x, float)) | (not isinstance(var_y, float)) |\
         (not isinstance(var_r, float)):
            print("Argument(s) non conforme(s)")
            return -3
        #On verifie que l'id est bien un string
        if not isinstance(var_id, str):
            print("l'id doit etre un str")
            return -3
        #Si l'id existe deja  ce n'est pas valide
        if self.entites.get(var_id) is not None:
            #Eventuellement remplacer avec un message
            #de type erreur de logging
            print("Clef deja existante")
            return -1
        #Si le rayon est < 0 ce n'est pas valide
        if var_r <= 0:
            print("Le rayon doit etre > 0")
            return -2
        #Sinon on ajoute la position
        self.entites[var_id] = (var_x, var_y, var_r)
        return 0


    def remove_position(self, var_id):
        """Retire la position dont la clef est l'argument"""
        if not isinstance(var_id, str):
            print("l'id doit etre un str")
            return -1
        #Si l'id n'existe pas dans le dictionnaire
        if self.entites.get(var_id) is None:
            #Eventuellement remplacer avec un message
            #de type erreur de logging
            print("Clef inconnue")
            return -2
        #Sinon on retire du dictionnaire la clef id et la valeur associee
        self.entites.pop(var_id)
        return 0

    def list_position(self):
        """Renvoie un str de la forme
        id1 : (x1,y1,r1)
        id2 : (x2,y2,r2)
        ...
        """

        #Initialise le str de retour
        retour = ""

        #Pour chaque entree du dictionnaire creer la ligne de l'affichage
        for i in self.entites:
            retour += i
            retour += " : "
            retour += str(self.entites[i])
            retour += '\n'

        #Cas ou il n'y a aucune position
        if retour == "":
            retour = "Aucune position enregistree"

        return retour

    def get_position(self, var_id):
        """Renvoie les coordonnees correspondant a l'id
        passe en argument dans un tuple de la forme
        (x,y)
        """
        if not isinstance(var_id, str):
            print("l'id doit etre un str")
            return -1

        #Si l'id n'existe pas dans le dictionnaire
        if self.entites.get(var_id) is None:
            #Eventuellement remplacer avec un message
            #de type erreur de logging
            print("Clef inconnue")
            return None

        #On ne renvoie que les deux premieres valeurs
        return self.entites.get(var_id)[:2]


    def get_distance(self, var_id1, var_id2):
        """Renvoie la distance euclidienne entre
        deux positions"""
        #Si les arguments ne sont pas conformes
        if (not isinstance(var_id1, str)) | (not isinstance(var_id2, str)):
            print("Les id doivent etre des str")
            return None

        #Si une ou les deux clef(s) ne sont pas connues
        if (self.entites.get(var_id1) is None) | (self.entites.get(var_id2) is None):
            print("Clef(s) inconnue(s)")
            return None

        var_x1 = self.entites.get(var_id1)[0]
        var_x2 = self.entites.get(var_id2)[0]
        var_y1 = self.entites.get(var_id1)[1]
        var_y2 = self.entites.get(var_id2)[1]

        dist_quad = (var_x1-var_x2)**2 + (var_y1-var_y2)**2
        return pow(dist_quad, 0.5)

    def is_collision(self, var_id1, var_id2):
        """Renvoie s'il y a collision ou non entre deux
        positions (booleen)"""
        #Appel d'une methode permet de faire les test
        #de validite et de recuperer la distance
        var_distance = self.get_distance(var_id1, var_id2)

        if var_distance is None:
            return -1

        var_r = self.entites.get(var_id1)[2] + self.entites.get(var_id2)[2]

        #Contact est une collision
        return var_distance < var_r

class SafeGrid(Grid):
    """Herite de Grid. Propose des methode d'ajout avec un controle de collision"""
    def __init__(self):
        """Constructeur"""
        self.entites = {}


    def __provoque_collision(self, var_x, var_y, var_r):
        """Permet de savoir si la position en argument provoque une collision"""

        for i in self.entites:
            pos_x = self.entites.get(i)[0]
            pos_y = self.entites.get(i)[1]
            pos_r = self.entites.get(i)[2]
            dist = pow((var_x-pos_x)**2 + (var_y-pos_y)**2, 0.5)
            if dist < pos_r + var_r:
                return True

        return False

    def Safe_add_position(self, var_id, var_x, var_y, var_r):
        """Ajout d'une position si cela n'entraine pas de collision""" 

        if self.__provoque_collision(var_x, var_y, var_r):
            print("Collision")
            return -1

        self.add_position(var_id, var_x, var_y, var_r)
        return 0

    def Safe_new_position(self, var_id, var_new_x, var_new_y):
        """Deplacement d'une position si cela n'entraine pas de collision"""
        if not isinstance(var_id, str):
            print("l'id doit etre un str")
            return -1

        #Si l'id n'existe pas dans le dictionnaire
        if self.entites.get(var_id) is None:
            #Eventuellement remplacer avec un message
            #de type erreur de logging
            print("Clef inconnue")
            return -1

        #On verifie avoir des float
        if (not isinstance(var_new_x, float)) | (not isinstance(var_new_y, float)):
            print("Argument(s) non conforme(s)")
            return -1

        #On verifie qu'il n'y a pas de collision avec la nouvelle position
        var_r = self.entites.get(var_id)[2]
        coord = self.entites.pop(var_id)
        if self.__provoque_collision(var_new_x, var_new_y, var_r):
            print("Collision")
            self.add_position(var_id, coord[0], coord[1], coord[2])
            return -1

        self.add_position(var_id, var_new_x, var_new_y, var_r)
        return 0
